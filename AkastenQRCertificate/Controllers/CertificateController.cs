﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AkastenQRCertificate.Extensions;
using AkastenQRCertificate.Services.Meta;
using AkastenQRCertificate.ViewsModel;
using PuppeteerSharp;
using PuppeteerSharp.Media;
using QRCoder;

namespace AkastenQRCertificate.Controllers
{
    [Route("/api/cert")]
    public class CertificateController : ControllerBase
    {
        private readonly ITemplateService _templateService;

        public CertificateController(ITemplateService templateService)
        {
            _templateService = templateService ?? throw new ArgumentNullException(nameof(templateService));
        }

        [HttpGet]
        public async Task<IActionResult> Cert()
        {
            var details = new CertificateModel
            {
                VehicleRegistrationNo = "KDC444N",
                CertificateNo = "123123123",
                OwnerPhoneNo = "0724486915",
                Status = "NEW INSTALLATION",
                DateOfInstallation = DateTime.Now.ToString("MM/dd/yyyy")
            };
            QRCodeGenerator QrGenerator = new QRCodeGenerator();
            var QRCodeText = $"AKASTEN GLOBAL LTD \n www.deakasten.com \n Tel: 0798035606/0732440037 \n email: akastenglobal@gmail.com \n STATUS: {details.Status} \n VEHICLE REG NO: {details.VehicleRegistrationNo} \n CERT NO: {details.CertificateNo}  \n INSTALLATION DATE: {details.DateOfInstallation} \n EXPIRE DATE: 12/12/2022 \n OWNER PHONE NO: {details.OwnerPhoneNo}";
            QRCodeData QrCodeInfo = QrGenerator.CreateQrCode(QRCodeText, QRCodeGenerator.ECCLevel.Q);
            QRCode QrCode = new QRCode(QrCodeInfo);
            Bitmap QrBitmap = QrCode.GetGraphic(60);
            byte[] BitmapArray = QrBitmap.BitmapToByteArray();
            string QrUri = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(BitmapArray));


            var model = new CertificateModel
            {
                VehicleRegistrationNo = details.VehicleRegistrationNo,
                ChassisNo = "KJP009P",
                Make = "Mazda",
                Type = "Atenza",
                DeviceSerialNumber = "1245785488",
                CertificateNo = details.CertificateNo,
                Location = "Nairobi",
                DateOfInstallation = details.DateOfInstallation,
                ExpiryDate = "12/12/2022",
                VehicleOwnerName = "Douglas Wanjala",
                VehicleOwnerId = "28304423",
                OwnerPhoneNo = details.OwnerPhoneNo,
                QRCode = QrUri,
                Status = details.Status
            };
            var html = await _templateService.RenderAsync("Templates/CertificateTemplate", model);
            await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true,
                ExecutablePath = PuppeteerExtensions.ExecutablePath
            });
            await using var page = await browser.NewPageAsync();
            await page.EmulateMediaTypeAsync(MediaType.Screen);
            await page.SetContentAsync(html);
            var pdfContent = await page.PdfStreamAsync(new PdfOptions
            {
                Format = PaperFormat.A4,
                PrintBackground = true,
                Landscape = true
            });
            return File(pdfContent, "application/pdf", $"Certificate-{model.VehicleRegistrationNo}.pdf"); 
        }
    }

    //Extension method to convert Bitmap to Byte Array
    public static class BitmapExtension
    {
        public static byte[] BitmapToByteArray(this Bitmap bitmap)
        {
            using MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Png);
            return ms.ToArray();
        }
    }
}