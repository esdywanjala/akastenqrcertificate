﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AkastenQRCertificate.ViewsModel
{
    public class CertificateModel
    {
        public string VehicleRegistrationNo { get; set; }

        public string ChassisNo { get; set; }

        public string Make { get; set; }

        public string Type { get; set; }

        public string DeviceSerialNumber { get; set; }

        public string OwnerPhoneNo { get; set; }

        public string VehicleOwnerName { get; set; }

        public string VehicleOwnerId { get; set; }

        public string CertificateNo { get; set; }

        public string Location { get; set; }

        public string DateOfInstallation { get; set; }

        public DateTime DateOfRenewal { get; set; }

        public string ExpiryDate { get; set; }

        public string QRCode { get; set; }

        public string Status { get; set; }

    }

 
}
