#pragma checksum "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0930f38f73d0470cb29ac47c3aa97ede63b3c913"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\_ViewImports.cshtml"
using AkastenQRCertificate;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\_ViewImports.cshtml"
using AkastenQRCertificate.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\Home\Index.cshtml"
using Microsoft.AspNetCore.Html;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0930f38f73d0470cb29ac47c3aa97ede63b3c913", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"efc1be27f9f612328bf514afe04e9f5f03883d19", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<AkastenQRCertificate.ViewsModel.CertificateModel>
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n");
#nullable restore
#line 8 "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\Home\Index.cshtml"
  
    Layout = null;

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<!doctype html>\r\n<html>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0930f38f73d0470cb29ac47c3aa97ede63b3c9133935", async() => {
                WriteLiteral(@"
    <meta charset=""utf-8"">
    <meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8"">
    <title>Certificate</title>
    <link rel=""stylesheet"" href=""https://localhost:44331/lib/bootstrap/dist/css/bootstrap.min.css"" />
    <link rel=""stylesheet"" href=""https://localhost:44331/css/cert-styles.css"" />
");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0930f38f73d0470cb29ac47c3aa97ede63b3c9135244", async() => {
                WriteLiteral(@"
    <div class=""row d-flex justify-content-end"">
        <div class=""qr-code"">
            <img width=""88"" height=""88"" src=""Model.QRCode"" class=""qr-img"" />
            <span class=""tf-bold tf-cert-size ff-footlight justify-content-center"" style=""font-family: FootlightMTLight;""> CERT NO: Model.CertificateNo</span>
        </div>
    </div>
    <div class=""row"">

        <div class=""col-md-4"">
            <!--Receipt Section-->
            <div class=""row receipt d-flex justify-content-center"">
                <span class=""tf-bold fs-13"" style=""font-family: 'GillSansUltraBold';"">ORIGINAL RECEIPT</span>
            </div>
            <div class=""receipt-details"">
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;"">Date:</span><span class=""dashed-underline"" style=""font-family: 'DilleniaUPC'; width: 57%; position: relative; top: -7px;""><span class=""ff-britannic"" style=""font-family:Britannic; fo");
                WriteLiteral(@"nt-size:12px; position:relative;top:3px;""> Model.DateOfInstallation</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;""> For Vehicle Reg No.: </span><span class=""dashed-underline line-width-48"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.VehicleRegistrationNo</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;"">Speed governer fitted and payment received </span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45 fs-13"" style=""font-family: Consolas;"">
                        <span class=""fs-13"" style=""font-family: Consolas;""> Sign:</span><span class=""dashed-underlin");
                WriteLiteral(@"e line-width-70"" style=""font-family: 'DilleniaUPC';  ""><span class=""detail-entry"">_</span></span>
                    </div>
                </div>
            </div>
            <!-- Receipt section -->
            &nbsp;
            <!--Receipt Section-->
            <div class=""row receipt d-flex justify-content-center"">
                <span class=""tf-bold fs-13"" style=""font-family: 'GillSansUltraBold';"">DUPLICATE ORIGINAL</span>
            </div>
            <div class=""receipt-details"">
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;"">Date:</span><span class=""dashed-underline"" style=""font-family: 'DilleniaUPC'; width: 57%; position: relative; top: -7px;""><span class=""ff-britannic"" style=""font-family:Britannic; font-size:12px; position:relative;top:3px;""> HRM-1</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div");
                WriteLiteral(@" class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;""> For Vehicle Reg No.: </span><span class=""dashed-underline line-width-48"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.VehicleRegistrationNo</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;"">Speed governer fitted and payment received  </span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45 fs-13"" style=""font-family: Consolas;"">
                        <span class=""fs-13"" style=""font-family: Consolas;""> Sign:</span><span class=""dashed-underline line-width-70"" style=""font-family: 'DilleniaUPC';  ""><span class=""detail-entry"">_</span></span>
                    </div>
                </div>
            </div>
            <!-- Recei");
                WriteLiteral(@"pt section -->
            <div class=""row qr-compliance"">
                <img width=""88"" height=""88"" src=""Model.QRCode"" class=""qr-img"" />
            </div>
            <div class=""row compliance d-flex justify-content-center"">
                <span class=""tf-bold fs-13"" style=""font-family: 'GillSansUltraBold';"">SPEED LIMITER COMPLIANCE</span>
            </div>
            <div class=""compliance-details"">
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;"">GOVERNOR TYPE/MODE:</span><span class=""dashed-underline"" style=""font-family: 'DilleniaUPC'; width: 57%; position: relative; top: -7px;""><span class=""ff-britannic"" style=""font-family:Britannic; font-size:12px; position:relative;top:3px;""> HRM-1</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""f");
                WriteLiteral(@"ont-family: Consolas;""> VEHICLE REGISTRATION NO: </span><span class=""dashed-underline line-width-48"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.VehicleRegistrationNo</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45"">
                        <span class=""fs-13"" style=""font-family: Consolas;"">
                            DATE OF INSTALLATION
                        </span>
                        <img width=""20"" height=""20"" src=""https://localhost:44331/images/check.png""");
                BeginWriteAttribute("class", " class=\"", 6264, "\"", 6272, 0);
                EndWriteAttribute();
                WriteLiteral(@" />
                        <span class=""fs-13"" style=""font-family: Consolas; position: relative; left: -14px; "">
                            RENEWAL
                        </span>
                        <svg version=""1.0"" id=""checkbox"" width=""30"" height=""32"" class=""check-box"" xmlns=""http://www.w3.org/2000/svg""
                             xmlns:xlink=""http://www.w3.org/1999/xlink"" x=""0px"" y=""0px""
                             viewBox=""209.667 9.667 589.37 540.667"" enable-background=""new 209.667 9.667 589.37 540.667""
                             xml:space=""preserve"">
                        <rect id=""checkbox_1_"" x=""211.667"" y=""11.667"" fill=""#FFFFFF"" stroke=""#000000"" stroke-width=""4""
                              stroke-miterlimit=""10"" width=""425.667"" height=""370.667"" />
                        <polygon id=""checkmark"" stroke=""#000000"" stroke-miterlimit=""10"" points=""451.92,291.724 254.569,205.919 211.667,220.22
                    451.92,463.333 798,28.589 737.937,28.589 "" />
                     ");
                WriteLiteral(@"       </svg>
                        <span class=""dashed-underline line-width-18"" style=""font-family: 'DilleniaUPC'; position: relative; left: -14px; ""><span class=""detail-entry"">Model.DateOfInstallation</span></span>
                    </div>
                </div>
                <div class=""row"">
                    <div class=""col-12 ml-45 fs-13"" style=""font-family: Consolas;"">
                        <span class=""fs-13"" style=""font-family: Consolas;""> EXPIRY DATE:</span><span class=""dashed-underline line-width-70"" style=""font-family: 'DilleniaUPC';  ""><span class=""detail-entry"">Model.ExpiryDate</span></span>
                    </div>
                </div>
            </div>
        </div>

        <div class=""col-md-8 cert-details"">
            <div class=""row d-flex justify-content-center mb-4"">
                <span class=""ff-cassandra"" style=""font-family:'Cassandra Personal Use';"">This is to certify that</span>
                <br />
            </div>
            <div class=""row""");
                WriteLiteral(@">
                <div class=""col-6"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">Vehicle Registration No:</span> <span class=""dashed-underline line-width-64"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.VehicleRegistrationNo</span></span>
                </div>
                <div class=""col-6"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">
                        Chassis
                        No:
                    </span> <span class=""dashed-underline line-width-64"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.ChassisNo</span></span>
                </div>

            </div>
            <div class=""row"">
                <div class=""col-6"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">Make:</span> <span class=""dashed-underline line-width-90"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.Make</span></span>
  ");
                WriteLiteral(@"              </div>
                <div class=""col-6"" style=""font-family:'Iskoola Pota';"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">Type:</span> <span class=""dashed-underline line-width-72"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.Type</span></span>
                </div>
            </div>
            <div class=""row"">
                <div class=""col-12"">
                    <span class=""ff-iskpota"">
                        Has been Fitted with approved Speed Governor (HRM-1) Hybrid Road
                        Master
                    </span>
                </div>
            </div>
            <div class=""row"">
                <div class=""col-12"" style=""font-family:'Iskoola Pota';"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">Device Serial Number:</span> <span class=""dashed-underline line-width-75"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.DeviceSerialNum");
                WriteLiteral(@"ber</span></span>
                </div>
            </div>
            <div class=""row"">
                <div class=""col-12"">
                    <span class=""tf-bold ti-30"" style=""font-family: 'BodoniMTBlack'; font-size: 20px;"">
                        The Governor is calibrated and sealed not to exceed 80
                        km/hr
                    </span>
                </div>
            </div>
            <div class=""row mb-2"">
                <div class=""col-12"">
                    <span class=""tf-bold tf-italic ti-88 ff-tns"">
                        <span class=""fs-15"">This Certificate is valid for 12 months from date of issue</span><br />
                        <span class=""fs-13"">
                            Tampering with the
                            speed
                            governor
                            is
                            an offence
                            and such will nullify all warranties and conditions.
                        ");
                WriteLiteral(@"</span>
                    </span>
                </div>
            </div>
            <div class=""row d-flex justify-content-center"">
                <span class=""ff-copperplate"" style=""font-family: Copperplate Gothic Light;"">FITTING CENTER DETAILS</span>
            </div>
            <div class=""row g-2"">
                <div class=""col-10"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">
                        Business Reg. No.
                    </span> <span class=""dashed-underline line-width-22"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">PVT-9XULXGL</span></span>

                    &nbsp; <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">
                        Pin No.:
                    </span> <span class=""dashed-underline line-width-22"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">P051766422K</span></span>
                    &nbsp;

                    <span class=""ff-iskpota"" style=""fon");
                WriteLiteral(@"t-family:'Iskoola Pota';"">VAT No:</span> <span class=""dashed-underline line-width-22"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">P051766422K</span></span>
                </div>
            </div>
            <div class=""row"">
                <div class=""col-12"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">
                        Location:
                    </span> <span class=""dashed-underline line-width-35"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.Location</span></span>


                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">
                        Tel:
                    </span> <span class=""dashed-underline line-width-20"" style=""font-family: 'DilleniaUPC'; "">
                        <span class=""detail-entry"">
                            0732440037,
                            0798035606
                        </span>
                    </span>
                </div>
");
                WriteLiteral(@"            </div>
            <div class=""row"">
                <div class=""col-12"">
                    <span class=""ff-iskpota"" style=""font-family:'Iskoola Pota';"">
                        Date of Installation
                    </span>
                    &nbsp;
                    <svg width=""38"" height=""28"">
                        <rect width=""28"" height=""18"" style=""fill:rgb(255,255,255);stroke-width:2;stroke:rgb(0,0,0)"" />
                    </svg>
                    <span class=""dashed-underline line-width-45"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">");
#nullable restore
#line 240 "E:\dotnet\akastenqrcertificate\AkastenQRCertificate\Views\Home\Index.cshtml"
                                                                                                                             Write("RENEWAL" == "RENEWAL" ? new HtmlString("Model.DateOfInstallation") : new HtmlString("NILL"));

#line default
#line hidden
#nullable disable
                WriteLiteral(@" </span></span>
                </div>
            </div>
            <div class=""row"">
                <div class=""col-12"">
                    <span style=""font-family:'Iskoola Pota';"">
                        Date of Renewal
                    </span> &nbsp;
                    <svg version=""1.0"" id=""checkbox"" width=""38"" height=""28"" xmlns=""http://www.w3.org/2000/svg""
                         xmlns:xlink=""http://www.w3.org/1999/xlink"" x=""0px"" y=""0px""
                         viewBox=""209.667 9.667 589.37 540.667"" enable-background=""new 209.667 9.667 589.37 540.667""
                         xml:space=""preserve"">
                    <rect id=""checkbox_1_"" x=""211.667"" y=""11.667"" fill=""#FFFFFF"" stroke=""#000000"" stroke-width=""4""
                          stroke-miterlimit=""10"" width=""425.667"" height=""480.667"" />
                    <polygon id=""checkmark"" stroke=""#000000"" stroke-miterlimit=""10"" points=""451.92,291.724 254.569,205.919 211.667,220.22
                    451.92,463.333 798,28.589 737");
                WriteLiteral(@".937,28.589 "" />
                            </svg>
                    <span class=""dashed-underline line-width-50"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.DateOfRenewal</span></span>
                </div>
            </div>
            <div class=""row ti-88"">
                <div class=""col-12"">
                    <span style=""font-family:'Iskoola Pota';""> Expiry Date:</span><span class=""dashed-underline line-width-50"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"">Model.ExpiryDate</span></span>
                </div>
            </div>
            <div class=""row ti-88"">
                <div class=""col-12"">
                    <span style=""font-family:'Iskoola Pota';"">AGL Signature:</span><span class=""dashed-underline line-width-50""><span class=""detail-entry""></span></span>
                </div>
            </div>
        </div>
    </div>

    <div class=""row"">
        <div class=""col-8 warranty"">
            VEHICLE OWNER’S NAME: <span");
                WriteLiteral(@" class=""dashed-underline line-width-60 warranty-entry"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"" style=""position:relative; left:-31px;"">Model.VehicleOwnerName</span></span> <br>
            VEHICLE OWNER’S ID:<span class=""dashed-underline line-width-64 warranty-entry"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"" style=""position:relative; left:-27px;"">Model.VehicleOwnerId</span></span> <br>
            OWNER’S PHONE NUMBER: <span class=""dashed-underline line-width-57 warranty-entry"" style=""font-family: 'DilleniaUPC';""><span class=""detail-entry"" style=""position:relative; left:-42px;"">Model.OwnerPhoneNo</span></span> <br>
            VEHICLE REGISTRATION NUMBER: <span class=""dashed-underline line-width-50 warranty-entry"" style=""font-family: 'DilleniaUPC';""><span class=""detail-entry"" style=""position:relative; left:-37px;"">Model.VehicleRegistrationNo</span></span> <br>
            SPEED GOVERNOR SERIAL NO: <span class=""dashed-underline line-width-50 warranty-entry"" styl");
                WriteLiteral(@"e=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry"" style=""position:relative; left:-18px;"">Model.DeviceSerialNumber</span></span> <br>
            DATE OF FITTING: <span class=""dashed-underline line-width-65 warranty-entry"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry""> Model.DateOfInstallation</span></span> <br>
            CUSTOMER SIGNATURE <span class=""dashed-underline line-width-60"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry""> _</span></span> <br>
            AGL REPRESENTATIVE: <span class=""dashed-underline line-width-30"" style=""font-family: 'DilleniaUPC'; ""><span class=""detail-entry""> MUHAMMAD IBN MUHAMMAD</span></span>    SIGNATURE: <span class=""dashed-underline line-width-20"" style=""font-family: 'DilleniaUPC';""><span class=""detail-entry"" style=""position: relative; top: -1px; ""> <img width=""100"" height=""20"" src=""https://localhost:44331/images/muhammad.jpeg""");
                BeginWriteAttribute("class", " class=\"", 18136, "\"", 18144, 0);
                EndWriteAttribute();
                WriteLiteral("></span></span> <br>\r\n        </div>\r\n\r\n    </div>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<AkastenQRCertificate.ViewsModel.CertificateModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
