**PDF and QR Code Solution**

This solution allows for the generation of a certificate template in PDF format to allow for printing on the provided certificate copies. In addition, for security purposes, the solution generates a QR Code that identifies the certificate and can be used to verify the authenticity of the same.

**Requirements**

Two packages are used in the solution to help achieve the objectives. These packages can be found in the NuGet Package Manager by searching for their names. They are:

1.	PuppeteerSharp � 5.1.0
2.	QRCoder � 1.4.1

**Implementation**

The Certificatecontroller is responsible for the PDF generation by calling the �api/cert� endpoint. This is made possible by gathering the various details needed in the certificate through the ViewsModel/CertificateModel. 
The objects/fields of the certificate need to be fetched from the vehicle details during the installation or addition of the same. Once passed to the mode, the solution will pick the details and generate the certificate template as well as the QR Code depending on the vehicle�s details. These details include, vehicle registration number, chassis, model, make, owner number etc.

**Important Files to Note**

1.	Controllers/CertificateController.cs
2.	Extensions/PuppeteerExtensions.cs
3.	Services/Meta/ITemplateServices.cs
4.	Views/Templates/CertificateTemplace.cshtml
5.	ViewsModel/CertificateModel.cs
6.	Wwwroot/css/cert-styles.css
7.	Wwwroot/lib/font add all the fonts

**Needed Adjustments**

1.	In Startup.cs add the ITemplateService inside the ConfugureSevices function

   public void ConfigureServices(IServiceCollection services)
  {
    services.AddControllersWithViews();
    services.AddScoped<ITemplateService, RazorViewsTemplateService>();
   }
   
2.	Add all the necessary files listed above noting the need to change/adjust the namespaces.
3.	Replacing the hard coded values with real values (CertificateController Lines 29-56).
4. Replacing all the occurences of "https://localhost:44331/" with the base url of the site.

